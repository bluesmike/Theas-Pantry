# Thea's Pantry at Worcester State University

## Documentation

- Please see our [documentation on librefoodpantry.org](https://librefoodpantry.org/#/projects/Theas-Pantry/)

- The source for our documentation is in [/docs](docs).

---
Copyright &copy; 2019 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
